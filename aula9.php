<?php 

function BuscarEndereco($cep){

    $endereco = file_get_contents("https://viacep.com.br/ws/$cep/json/");
    return json_decode($endereco,true);

}

$endereco = BuscarEndereco("01001-000");

echo "Endereco: ".$endereco['logradouro']."<br>";
echo "bairro: ".$endereco['bairro']."<br>";
echo "Cidade: ".$endereco['localidade']."<br>";
echo "Estado: ".$endereco['uf']."<br>";
echo "Cep: ".$endereco['cep']."<br>";

?>