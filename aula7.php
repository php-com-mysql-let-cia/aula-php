<?php

$listaCompra = ["banana", "arroz","feijão"];

echo $listaCompra[1];

echo "<hr>";

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";
$usuario = [
    "nome" => "Edson Rodrigues",
    "e-mail" => "edson@teste.com"
];


echo $usuario['nome'];

echo "<hr>";

$usuarios = [
    [
        "nome" => "Edson Rodrigues",
        "email" => "edson@teste.com"
    ],
    [
        "nome" => "João",
        "email" => "joao@teste.com"
    ]
];

echo "<pre>";
print_r($usuarios);
echo "</pre>";

echo $usuarios[1]['nome'];

echo "<hr>";

foreach($listaCompra as $item){

    echo $item . ", ";

}


echo "<hr>";
            //        [0] => ['nome'=>'...']
foreach($usuarios as $key => $usuario){
    echo $key ." - ";
    echo $usuario['nome']. "<br>";
    echo $usuario['email']. "<br>";
}

?>