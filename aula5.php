<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
</head>

<body>

<h2>Envio por Get</h2>
    <form action="aula6.php" method="get">
        <p>
            <Label> Nome</Label>
            <input type="text" name="nome">

        </p>

        <p>
            <Label> Idade</Label>
            <input type="text" name="idade">
        </p>

        <p>
            <button type="submit">Cadastrar</button>
        </p>



    </form>

    <?php

    if (isset($_GET['nome']) && isset($_GET['idade'])) {
        echo $_GET['nome'];

        echo "<br>";

        echo $_GET['idade'];
        }

    ?>


<h2>Envio por Post</h2>
    <form action="aula6.php" method="post">
        <p>
            <Label> Nome</Label>
            <input type="text" name="nome">

        </p>

        <p>
            <Label> Idade</Label>
            <input type="text" name="idade">
        </p>

        <p>
            <button type="submit">Cadastrar</button>
        </p>



    </form>


    <?php

    if (isset($_POST['nome']) && isset($_POST['idade'])) {
        echo $_POST['nome'];

        echo "<br>";

        echo $_POST['idade'];
    }


    ?>

</body>


</html>